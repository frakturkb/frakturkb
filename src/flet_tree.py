from __future__ import annotations

from typing import Protocol

import flet as ft
import flet_core

THEME: ft.Theme = None


def set_theme(theme):
    global THEME
    THEME = theme


def get_theme() -> ft.Theme:
    global THEME
    if THEME is None:
        THEME = ft.theme.Theme(color_scheme_seed="green")
    return THEME


class LabelFactory(Protocol):
    def __call__(self, item: TreeItem) -> str | ft.Control:
        ...


class TreeItem(ft.UserControl):
    def __init__(self, content: str | ft.Control | LabelFactory, **properties):
        super().__init__()
        self.properties = properties
        self._content_factory = None
        self.content = None
        if callable(content):
            self._content_factory = content
        elif not isinstance(content, ft.Control):
            self.content = ft.Text(value=content)
        else:
            self.content = content

        header_border = None
        # children_border = ft.border.only(
        #     bottom=ft.border.BorderSide(1, ft.colors.PRIMARY_CONTAINER)
        # )
        children_border = None
        debug_indent = False
        if debug_indent:
            header_border = ft.border.all(1, ft.colors.PINK_600)
            children_border = ft.border.all(1, ft.colors.BLUE_600)

        indent_size = 40
        indent_scale = 1
        self.indent = indent_scale * indent_size

        self.expand_butt = ft.TextButton(
            text="+",
            on_click=self.toggle_expanded,
            visible=False,
            width=self.indent,
            # height=self.indent,
            # icon=ft.icons.ARROW_RIGHT_SHARP,
            # icon_color="blue400",
            # selected_icon=ft.icons.ARROW_DROP_DOWN_SHARP,
            # selected_icon_color="green400",
            # icon_size=self.indent * 0.75,
        )

        self.header = ft.Container(
            content=self.content,
            padding=ft.padding.only(left=self.indent),
            border=header_border,
        )
        self.children = ft.Column(
            [],
            visible=False,
        )
        self.children_container = ft.Container(
            content=self.children,
            margin=ft.margin.only(left=self.indent),
            border=children_border,
        )
        self.update_content()

    def update_content(self):
        if self._content_factory is None:
            return

        content = self._content_factory(self)
        if not isinstance(content, ft.Control):
            content = ft.Text(content)
        self.content = content
        self.header.content = self.content
        if self.header.page:
            self.header.update()

    def build(self):
        return ft.Container(
            content=ft.Column(
                [
                    ft.Row(
                        [self.expand_butt, self.header],
                        spacing=0,
                    ),
                    self.children_container,
                ],
            ),
            # border=ft.border.all(1),
        )

    def set_expanded(self, b: bool = True):
        if self.children.visible is not b:
            self.toggle_expanded()

    def toggle_expanded(self, e=None):
        if self.children.visible:
            self.children.visible = False
            # self.expand_butt.icon = ft.icons.ARROW_RIGHT_SHARP
            self.expand_butt.text = "+"
            self.expand_butt.update()
            self.header.margin = 50
        elif self.children.controls:
            self.children.visible = True
            # self.expand_butt.icon = ft.icons.ARROW_DROP_DOWN_SHARP
            self.expand_butt.text = "-"
            self.expand_butt.update()

        self.children.update()

    def add_item(self, label, **properties) -> TreeItem:
        # self.expander.content = self.expand_butt

        self.expand_butt.visible = True
        if self.expand_butt.page:
            self.expand_butt.update()

        self.header.padding = self.indent - self.expand_butt.width
        if self.header.page:
            self.header.update()

        item = TreeItem(label, **properties)
        self.children.controls.append(item)
        if self.children.page:
            self.children.update()

        return item


class TreeView(ft.ListView):
    def __init__(self, *args, **kwargs):
        defaults = dict(
            expand=True,
            spacing=10,
            first_item_prototype=False,
        )
        [kwargs.setdefault(k, v) for k, v in defaults.items()]
        super().__init__(*args, **kwargs)

    def add_item(self, label: str) -> TreeItem:
        item = TreeItem(label)
        self.controls.append(item)
        if self.page:
            self.scroll_to(offset=-1, duration=100)
        return item


def test():
    import random

    def random_add_items(parent: TreeItem, depth=0):
        if depth > 4:
            return
        for j in range(random.randint(0, 5)):
            dice = random.choice(["string", "custom control", "factory"])
            if dice == "factory":

                def content_factory(item: TreeItem):
                    item.add_item("My child !!!")
                    return "Progamatic for item " + str(item.properties["item_index"])

                label = content_factory
            elif dice == "custom control":
                label = ft.Row(
                    [
                        ft.Icon(name=ft.icons.HOME),
                        ft.Text(value=f"Item {j}"),
                        ft.PopupMenuButton(
                            tooltip="Actions",
                            items=[
                                ft.PopupMenuItem(
                                    icon=ft.icons.POWER_INPUT, text="Check power"
                                ),
                                ft.PopupMenuItem(),  # divider
                                ft.PopupMenuItem(
                                    text="Checked item",
                                    checked=False,
                                ),
                            ],
                        ),
                    ]
                )
            else:
                label = f"Child {j}"
            item = parent.add_item(label, item_index=j)
            if random.random() > 0.5:
                random_add_items(item, depth=depth + 1)

    def main(page: ft.Page):
        # page.theme = get_theme()
        set_theme(page.theme)

        tree_view = TreeView()
        page.add(tree_view)
        for i in range(30):
            root = tree_view.add_item(f"Line {i}")
            random_add_items(root)

    ft.app(target=main)


if __name__ == "__main__":
    test()
