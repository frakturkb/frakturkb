import flet as ft
from flet_tree import TreeView


class TaskControl(ft.UserControl):
    def __init__(self, title):
        super().__init__()
        self.title = title
        self.comments_value = ""

        self.title = ft.Checkbox(label=self.title)
        self.expander = ft.IconButton(
            icon=ft.icons.ARROW_RIGHT,
            selected_icon=ft.icons.ARROW_DROP_DOWN,
            icon_color="blue400",
            selected_icon_color="green400",
            icon_size=20,
            tooltip="Toggle comments",
            on_click=self.toggle_comments,
        )
        self.comments = ft.TextField(
            label="Comments",
            value=self.comments_value,
            multiline=True,
            visible=False,
        )

    def build(self):
        return ft.Column(
            [
                ft.Row([self.expander, self.title]),
                ft.Container(
                    content=self.comments,
                    margin=ft.margin.only(left=50),
                ),
            ]
        )

    def toggle_comments(self, e):
        if self.comments.visible:
            self.comments.visible = False
            self.expander.selected = False
            self.expander.update()
        else:
            self.comments.visible = True
            self.expander.selected = True
            self.expander.update()
            # self.comments.icon = ft.icons.ARROW_RIGHT_ALT

        self.comments.update()


def main(page: ft.Page):
    def add(title, parent=None):
        parent = parent or tasks_view
        popup = ft.PopupMenuButton()
        add_task = ft.IconButton(icon=ft.icons.ADD)
        header = ft.Row([ft.TextField(value=title), add_task])
        item = parent.add_item(header)
        add_task.on_click = lambda e, parent=item: add(new_task.value or "???", parent)

        popup.items = [
            ft.PopupMenuItem(
                icon=ft.icons.ADD_CIRCLE_OUTLINE,
                text="Add Task",
                on_click=lambda e, parent=item: add(new_task.value or "???", parent),
            )
        ]
        comments = ft.TextField(
            label="Comments",
            value="",
            multiline=True,
            visible=False,
        )
        item.add_item(comments)
        item.set_expanded()
        if parent.page:
            parent.update()

    def add_clicked(e):
        v = new_task.value.strip()
        if not v:
            return
        new_task.value = ""
        new_task.update()
        add(v)
        tasks_view.scroll_to(offset=-1, duration=100)

    new_task = ft.TextField(hint_text="Whats needs to be done?")
    tasks_view = TreeView(
        expand=True,
        spacing=10,
        first_item_prototype=True,
    )
    view = ft.Column(
        width=600,
        controls=[
            ft.Row(
                controls=[
                    new_task,
                    ft.FloatingActionButton(
                        text="Add Task",
                        icon=ft.icons.ADD,
                        on_click=add_clicked,
                    ),
                ],
            ),
        ],
    )

    page.horizontal_alignment = ft.CrossAxisAlignment.CENTER
    page.add(view, tasks_view)

    tasks_view.scroll_to(offset=-1, duration=1)


def xx(page: ft.Page):
    tasks_view = ft.ListView(expand=True, spacing=10)
    for i in range(5000):
        tasks_view.controls.append(ft.Text(f"Line {i}"))
    page.add(tasks_view)


ft.app(target=main)
