from __future__ import annotations

import typing
from typing import Optional, Type, TypeVar, Generic, Any
import contextlib

import pydantic
from pydantic_core import PydanticUndefined

from rich import print


class _Base(pydantic.BaseModel):
    _name: Optional[str] = None
    _parent: Optional[_Base] = None
    _path_sep: str = "/"

    @pydantic.model_validator(mode="before")
    @classmethod
    def _defaults_from_annotations(cls, data: Any) -> Any:
        if not isinstance(data, dict):
            data = {}
        for name, field in cls.model_fields.items():
            if name not in data:
                default = field.get_default()
                if default is PydanticUndefined:
                    # print(">>> AutoDefault", name, field.annotation)
                    data[name] = field.annotation()
        return data

    @pydantic.model_validator(mode="after")
    def _set_names_and_parents(self) -> _Base:
        for name, field in self:
            if isinstance(field, _Base):
                field._name = name
                field._parent = self
        return self

    def path(self) -> str:
        parent = self._parent
        if parent is None:
            return self._path_sep + (str(self._name) or "")
        # return parent.path() + parent._path_sep + str(self._name)
        return parent._get_child_path(self)

    def _get_child_path(self, child):
        return self.path() + self._path_sep + str(child._name)

    def getter(self):
        parent = self._parent
        if parent is None:
            return self._name or "<Root>"
        return parent._get_child_getter(self, terminal=True)

    def _get_child_getter(self, child, terminal: bool):
        me = self._name
        if self._parent is not None:
            me = self._parent._get_child_getter(self, terminal=False)
        return me + f".{child._name}"

    def __getitem__(self, slot_name: str):
        field = getattr(self, slot_name)
        if isinstance(field, Slot):
            return field.connected
        else:
            raise TypeError(f"The field {slot_name!r} is not a Slot but {field!r}")

    def _on_node_connected(self, node: Node, to_slot: Slot):
        pass

    def _on_node_disconnecting(self, node: Node, from_slot: Slot):
        pass


class Node(_Base):
    def _get_child_getter(self, child, terminal: bool):
        me = str(self._name)
        if self._parent is not None:
            me = self._parent._get_child_getter(self, terminal=False)
        if not terminal and isinstance(child, Slot):
            return me + f"\['{child._name}']"
        return me + "." + str(child._name)

    def on_connected(self, to_slot: Slot):
        visitor = Caller()
        visitor.set_pre_call("_on_node_connected", self, to_slot)
        visitor(self)

    def on_disconnecting(self, from_slot: Slot):
        visitor = Caller()
        visitor.set_post_call("_on_node_disconnecting", self, from_slot)
        visitor(self)


class Slot(_Base):
    _path_sep: str = ":"
    _connectables: list[Type[Node]] = [Node]
    connected: Optional[Node] = None
    loc: tuple[float, float, float] = (0, 0, 0)
    rot: tuple[float, float, float] = (0, 0, 0)

    # def path(self) -> str:
    #     parent = self._parent
    #     path = parent and parent.path() or ""
    #     return path + f"\[{self._name}]"

    def _get_child_path(self, child):
        return self.path()  # + self._path_sep + str(child._name)

    def _get_child_getter(self, child, terminal: bool):
        me = self._name
        if self._parent is not None:
            me = self._parent._get_child_getter(self, False)
        return me

    def get_connectable_types(self) -> list[Type[Node]]:
        return self._connectables

    def make_new_node_name(self, NodeType: Type[Node]) -> str:
        return NodeType.__name__

    def assert_allowed_type(self, candidat: Type[Node]):
        connectables = self.get_connectable_types()
        for NodeType in connectables:
            if issubclass(candidat, NodeType):
                return
        raise TypeError(f"invalid Node type {candidat}, must be one of {connectables}.")

    def connect(
        self, NodeType: Type[Node], **node_attributes: Optional[dict[str, Any]]
    ) -> Node:
        self.assert_allowed_type(NodeType)
        if self.connected is not None:
            self.connected.on_disconnecting(self)
            self.connected._parent = None

        node = NodeType(**node_attributes)
        node._parent = self
        name = node_attributes.get("name") or self.make_new_node_name(NodeType)
        node._name = name
        self.connected = node
        node.on_connected(self)

        return node

    def move(self, x=0, y=0, z=0):
        self.loc = tuple([v + o for v, o in zip(self.loc, (x, y, z))])
        print(self.loc)


def SlotFor(*NodeTypes: Type[Node], **slot_attributes):
    slot = Slot(**slot_attributes)
    slot._connectable = NodeTypes
    return slot


class Slots(_Base):
    _path_sep: str = ":"
    _slot_type: Optional[Slot] = None
    _slot_connectable: list[Type[Node]] = [Node]
    slots: dict[str, Slot] = {}

    def _get_child_getter(self, child, terminal=False):
        me = self._name
        if self._parent is not None:
            me = self._parent._get_child_getter(self, False)
        return me + f"\[{child._name!r}]"

    def make_new_slot_name(self, slot: Slot):
        return len(self.slots)

    def connect(
        self, NodeType: type[Node], **node_attributes: Optional[dict[str, Any]]
    ) -> Node:
        slot = self._slot_type()
        slot._connectable = list(self._slot_connectable)
        slot._parent = self
        name = node_attributes.get("name") or self.make_new_slot_name(slot)
        slot._name = name
        self.slots[name] = slot
        try:
            node = slot.connect(NodeType, **node_attributes)
        except TypeError:
            del self.slots[name]
            raise
        return node

    def __getitem__(self, key):
        return self.slots[key].connected


def SlotsFor(*NodeTypes: list[Type[Node]], slot_type: Type[Slot] = Slot):
    slots = Slots()
    slots._slot_type = slot_type
    slots._slot_connectable = NodeTypes
    return slots


class Visitor:
    def __init__(self) -> None:
        self._depth = 0

    def __call__(self, field, field_name: str = "<ROOT>"):
        return self.visit(field, field_name)

    def visit(self, field, field_name: str = "<ROOT>"):
        if isinstance(field, _Base):
            self._pre(field_name, field)

            with self._enter_field(field):
                for name, child in field:
                    if isinstance(field, Slots) and name == "slots":
                        with self._enter_collection(name):
                            for k, v in child.items():
                                self.visit(v, k)
                    else:
                        self.visit(child, name)

            self._post(field_name, field)
        else:
            self._do_value(field_name, field)

    @contextlib.contextmanager
    def _enter_field(self, field: _Base):
        self._depth += 1
        self._entered(field)
        yield
        self._depth -= 1
        self._left(field)

    @contextlib.contextmanager
    def _enter_collection(self, name: str):
        self._collection_entered(name)
        self._depth += 1
        yield
        self._depth -= 1
        self._collection_left(name)

    def _pre(self, name: str, field: _Base):
        pass

    def _entered(self, field: _Base):
        pass

    def _collection_entered(self, name):
        pass

    def _do_value(self, name: str, field: pydantic.Field):
        pass

    def _collection_left(self, name):
        pass

    def _left(self, field: _Base):
        pass

    def _post(self, name: str, field: _Base):
        pass


class Printer(Visitor):
    def __init__(self, show_getter: bool = True) -> None:
        super().__init__()
        self._show_getter = show_getter

    def indent_str(self):
        return "  "

    def current_indent(self):
        return self._depth * self.indent_str()

    def _pre(self, name: str, field: _Base):
        prefix = str(name) and (str(name) + "=") or ""
        if self._show_getter:
            field_info = field.getter()
        else:
            field_info = field.path()
        print(f"{self.current_indent()}{prefix}<{type(field).__name__} {field_info}>")

    def _collection_entered(self, name):
        print(f"{self.current_indent()}[")

    def _do_value(self, name: str, value):
        print(self.current_indent() + name + "=" + str(value))

    def _collection_left(self, name):
        print(f"{self.current_indent()}]")


class Caller(Visitor):
    def __init__(self) -> None:
        super().__init__()
        self._pre_callable_name: str | None = None
        self._pre_call_args = ()
        self._pre_call_kwargs = {}

        self._post_callable_name: str | None = None
        self._post_call_args = ()
        self._post_call_kwargs = {}

    def set_pre_call(self, callable_name, *call_args, **call_kwargs):
        self._pre_callable_name = callable_name
        self._pre_call_args = call_args
        self._pre_call_kwargs = call_kwargs

    def set_post_call(self, callable_name, *call_args, **call_kwargs):
        self._post_callable_name = callable_name
        self._post_call_args = call_args
        self._post_call_kwargs = call_kwargs

    def _pre(self, name, field: _Base):
        if self._pre_callable_name is not None:
            callable = getattr(field, self._pre_callable_name)
            callable(*self._pre_call_args, **self._pre_call_kwargs)

    def _post(self, name, field: _Base):
        if self._post_callable_name is not None:
            callable = getattr(field, self._post_callable_name)
            callable(*self._post_call_args, **self._post_call_kwargs)


def test():
    # --- Structure

    class Finger(Node):
        pass

    class Thumb(Finger):
        pass

    class Hand(Node):
        thumb: Thumb
        fore_finger: Finger
        middle_finger: Finger
        ring_finger: Finger
        little_finger: Finger

    class Foot(Node):
        finger_count: int = 5
        ik_switche: bool = True

    class Arm(Node):
        # hand: Slot = SlotFor(Hand)
        hand: Hand

    class Leg(Node):
        # foot: Slot = SlotFor(Foot)
        foot: Foot

    class BipedTrunk(Node):
        arm_left: Slot = SlotFor(Arm)
        arm_right: Slot = SlotFor(Arm)
        leg_left: Slot = SlotFor(Leg)
        leg_right: Slot = SlotFor(Leg)

    class MultipedTrunk(Node):
        arms: Slots = SlotsFor(Arm)
        legs: Slots = SlotsFor(Leg)

    class _Tail(Node):
        len: int

    class ShortTail(_Tail):
        len: int = 2

    class LongTail(_Tail):
        len: int = 10

    class Root(Node):
        _name: str = "root"
        trunk: Slot = SlotFor(BipedTrunk, MultipedTrunk)
        tail: Slot = SlotFor(ShortTail, LongTail)

    # --- Builds

    root = Root()
    if 1:
        trunk: MultipedTrunk = root.trunk.connect(MultipedTrunk)
        trunk.arms.connect(Arm)
        trunk.arms.connect(Arm)
        trunk.arms.connect(Arm)
        trunk.legs.connect(Leg, name="LegLeft1")
        trunk.legs.connect(Leg, name="LegLeft2")
        trunk.legs.connect(Leg, name="LegRight1")
        leg_right_2 = trunk.legs.connect(Leg, name="LegRight2")
        print("--->", root["trunk"].legs["LegRight2"].foot)
        print("--->", root["trunk"].legs["LegRight2"].foot.path())
        print("--->", root["trunk"].legs["LegRight2"].foot.getter())

    else:
        trunk: BipedTrunk = root.trunk.connect(BipedTrunk)
        root.trunk.connected.leg_left.connect(Leg)
        root.trunk.connected.leg_right.connect(Leg)

    printer = Printer(show_getter=True)
    printer(root)


if __name__ == "__main__":
    test()
