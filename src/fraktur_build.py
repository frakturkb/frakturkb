from __future__ import annotations

from typing import TypeVar, Type, Optional, Any, Literal

from rich import print

from pydantree import Node, Slot, Slots, SlotFor, SlotsFor, Printer


class MCU:
    pass


class Part(Node):
    def find_MCU(self) -> MCU | None:
        self._find_MCU(set())

    def _find_MCU(self, seen: set) -> MCU | None:
        if isinstance(self, _MCUNode):
            return self
        for name, field in self:
            if field in seen:
                continue
            seen.add(field)
            try:
                MCU = field.find_MCU()
            except TypeError:
                MCU = None
            if MCU is not None:
                return MCU

        return self._parent_slot._parent_node._find_MCU(seen)


class _MCUNode(Part):
    pass


# ---------------------
# ---------------------
# ---------------------


class SwitchesSleeving(Part):
    wire: Literal[
        "Black", "Red", "Blue", "Yellow", "Orange", "Purple", "Kaki"
    ] = "Black"
    connection: Literal["Black", "Red", "Blue", "Yellow"] = "Black"


class Switch(Part):
    style: Literal["MX", "Kailh Choc"] = "MX"


class ColSwitches(Part):
    switch1: Switch
    switch2: Switch
    switch3: Switch
    switch4: Switch
    sleeving: SwitchesSleeving


class ColHolder(Part):
    switches: ColSwitches  # = ColSwitches()


class Column(Part):
    holder: ColHolder  # = ColHolder()


class ColSlots(Slots):
    _max_col: int = 10
    _slot_type = Slot
    _slot_connectable = [Column]

    def connect(self, NodeType: type[Node], **node_attributes: dict[str, Any]) -> Node:
        if len(self.slots) > self._max_col:
            raise ValueError("Already full.")
        return super().connect(NodeType, **node_attributes)


HookSide = Literal[
    "Left", "Right", "Front Left", "Front Right", "Back Left", "Back Right"
]


class _Module(Part):
    def hook_side(self):
        return self._slot.hook_side


class ModuleSlot(Slot):
    hook_side: HookSide

    def get_connectable_types(self) -> list[Type[Node]]:
        return PalmRest, Arm, Cap


class PalmRest(_Module):
    left_hook: ModuleSlot = ModuleSlot(hook_side="Left")
    right_hook: ModuleSlot = ModuleSlot(hook_side="Right")


class Arm(_Module):
    left_hook: ModuleSlot = ModuleSlot(hook_side="Left")
    right_hook: ModuleSlot = ModuleSlot(hook_side="Right")


class Cap(_Module):
    pass


class ColRail(Part):
    columns: ColSlots = ColSlots(_max_col=8)
    left_hook: ModuleSlot = ModuleSlot(hook_side="Left")
    right_hook: ModuleSlot = ModuleSlot(hook_side="Right")
    front_left_hook: ModuleSlot = ModuleSlot(hook_side="Front Left")
    front_right_hook: ModuleSlot = ModuleSlot(hook_side="Front Right")
    back_left_hook: ModuleSlot = ModuleSlot(hook_side="Back Left")
    back_right_hook: ModuleSlot = ModuleSlot(hook_side="Back Right")

    def collect_wires(self):
        return (
            self.columns.collect_wires()
            + self.left_hook.collect_wires()
            + self.right_hook.collect_wires()
            + self.front_left_hook.collect_wires()
            + self.front_right_hook.collect_wires()
            + self.back_left_hook.collect_wires()
            + self.back_right_hook.collect_wires()
        )


class ColRailMini(ColRail):
    columns: ColSlots = ColSlots(_max_col=4)


class Desk(Part):
    _name = "desk"
    left_split: Slot = SlotFor(ColRail, ColRailMini, loc=(-10, 0, 0))
    right_split: Slot = SlotFor(ColRail, ColRailMini, loc=(10, 0, 0))

    def collect_wires(self):
        return self.left_split.collect_wires() + self.right_split.collect_wires()


# ---------------------
# ---------------------
# ---------------------


def test():
    build = Desk()

    left: ColRail = build.left_split.connect(ColRail)

    col1: Column = left.columns.connect(Column)
    col1.holder.switches.sleeving = SwitchesSleeving(wire="Orange", connection="Black")

    col2: Column = left.columns.connect(Column)
    col3: Column = left.columns.connect(Column)
    col4: Column = left.columns.connect(Column)
    col5: Column = left.columns.connect(Column)
    col6: Column = left.columns.connect(Column)

    left.left_hook.connect(Cap)
    palm_rest: PalmRest = left.right_hook.connect(PalmRest)
    palm_rest.right_hook.connect(Cap)

    right = build.right_split.connect(ColRail)

    # print(build)
    printer = Printer(show_getter=True)
    printer(build)


if __name__ == "__main__":
    test()
